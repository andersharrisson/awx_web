FROM ansible/awx_web:9.0.1

LABEL maintainer="anders.harrisson@esss.se"

USER 0

RUN sed -i 's/\(CLUSTER_HOST_ID =\).*/\1 os.getenv("CLUSTER_HOST_ID", "awx")/' /etc/tower/settings.py

# Install ESS internal CA Certificate
ADD http://artifactory.esss.lu.se/artifactory/certificates/esss-ca01-ca.cer /etc/pki/ca-trust/source/anchors/
RUN chmod 0755 /etc/pki/ca-trust/source/anchors/esss-ca01-ca.cer && \
    update-ca-trust

USER 1000
